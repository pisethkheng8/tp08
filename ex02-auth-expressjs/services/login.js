const { readUsers } = require("../db/db");

const login = (email, password) => {
  console.log(email, password);
  //Check if user and pwd are matched
  const users = readUsers();

  let index = -1;

  for (let i = 0; i < users.length; i++) {
    if (users[i].email == email && users[i].password == password) {
      index = i;
      break;
    }
  }
  //return user's info
  if (index == -1) {
    return { success: false, error: "The user's information is incorrect~" };
  } else {
    return users[index];
  }
};

module.exports = {
  login,
};

const { readUsers, writeUsers } = require("../db/db");

const register = (params) => {
  try {
    const { email, username, firstName, lastName, password } = params;
    //Read all user from database
    const users = readUsers();
    //check if user existed
    const existed = checkIfExist(params);
    if (existed.status == true) {
      throw "Something went wrong";
    }
    //Append user
    users.push(params);
    //Write users information into DB
    writeUsers(users);
    //Return the complete user's info
    return { success: true, data: params };
  } catch (error) {
    return { success: false, data: error };
  }
};

function checkIfExist(user) {
  console.log("check", user);
  const users = readUsers();
  console.log("check", users);

  let index = -1;

  for (let i = 0; i < users.length; i++) {
    if (users[i].email == user.email || users[i].username == user.username) {
      index = i;
      break;
    }
  }

  console.log("check ", index);

  if (index == -1) {
    return {
      status: false,
      data: [],
    };
  } else {
    return {
      status: true,
      data: users[index],
    };
  }
}
module.exports = {
  register,
};

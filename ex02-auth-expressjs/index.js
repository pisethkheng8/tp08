const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

app.use(require("./routes"));

app.listen(process.env.PORT || 3001, () => {
  console.log("Your app is running on http://localhost:3001");
});

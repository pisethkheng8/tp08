var express = require("express");
const { login } = require("../services/login");
const { register } = require("../services/register");
var router = express.Router();

router.get("/", function (req, res) {
  console.log("Router working");
  res.json({ text: "Hello from API testing" });
});

router.post("/login", function (req, res) {
  const { email, password } = req.body;
  const results = login(email, password);
  res.json(results);
});

router.post("/register", function (req, res) {
  const results = register(req.body);
  res.json(results);
});

module.exports = router;
